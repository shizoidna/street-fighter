import { DOMElementArgument } from '../entities/dom-element-argument';
import { DOMElementArgumentAttributes } from '../entities/dom-element-argument-attributes';

export function createElement({ tagName, className, attributes = {} }: DOMElementArgument ): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes)
      .forEach((key: string) => {
        const attr: string | undefined = attributes[key as keyof DOMElementArgumentAttributes];

        if(attr !== undefined && attr !== null) {
          element.setAttribute(key, attr);
        }
      });

  return element;
}
