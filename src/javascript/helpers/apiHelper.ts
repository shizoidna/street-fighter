import { fightersDetails, fighters, Fighter, FighterDetail } from './mockData';

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

async function callApi(endpoint: string, method: string): Promise<Fighter[] | FighterDetail> {
  const url: string = API_URL + endpoint;
  const options: {method: string} = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response: Response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result: HTMLMetaElement) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<Fighter[] | FighterDetail> {
  const response: Fighter[] | FighterDetail | undefined = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): FighterDetail | undefined {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: string = endpoint.substring(start + 1, end);

  return fightersDetails.find((it: FighterDetail) => it._id === id);
}

export { callApi };
