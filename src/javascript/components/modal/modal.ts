import { modalArgument } from '../../entities/modal-argument';
import { createElement } from '../../helpers/domHelper';

export function showModal({ title, bodyElement, onClose = () => {} }: modalArgument): void {
  const root: HTMLElement | null = getModalContainer();
  const modal: HTMLElement = createModal({ title, bodyElement, onClose });

  if (root === null) {
    console.log(`Root element doesn't exist`);
  } else {
    root.append(modal);
  }
}

function getModalContainer(): HTMLElement | null {
  return document.getElementById('root');
}

function createModal({ title, bodyElement, onClose }:  modalArgument): HTMLElement {
  const layer: HTMLElement = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer: HTMLElement = createElement({ tagName: 'div', className: 'modal-root' });
  const header: HTMLElement = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: (() => void) | undefined): HTMLElement {
  const headerElement: HTMLElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement: HTMLElement = createElement({ tagName: 'span' });
  const closeButton: HTMLElement = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    hideModal();
    if (onClose) {
      onClose();
    }
  }

  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal(): void {
  const modal: Element = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
