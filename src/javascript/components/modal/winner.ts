import { FighterDetail } from '../../helpers/mockData';
import { showModal } from './modal';

export function showWinnerModal(fighter: FighterDetail): void {
  showModal({
    title: `${fighter.name} Wins! Congratulations!`,
    bodyElement: ''
  });
}
