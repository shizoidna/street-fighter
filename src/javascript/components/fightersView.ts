import { ImageAttributes } from '../entities/image-attributes';
import { SelectFighter } from '../entities/select-fighter';
import { createElement } from '../helpers/domHelper';
import { Fighter, FighterDetail } from '../helpers/mockData';
import { createFightersSelector } from './fighterSelector';

export function createFighters(fighters: Fighter[]): HTMLElement {
  const selectFighter: SelectFighter = createFightersSelector();
  const container: HTMLElement = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList: HTMLElement = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements: HTMLElement[] = fighters.map((fighter: Fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: Fighter, selectFighter: SelectFighter): HTMLElement {
  const fighterElement: HTMLElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement: HTMLElement = createImage(fighter);
  const onClick = (event: Event) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: Fighter) {
  const { source, name }: Fighter = fighter;
  const attributes: ImageAttributes = {
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}
