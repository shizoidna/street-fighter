import { controls } from '../../constants/controls';
import { FighterDetail } from '../helpers/mockData';

let isFirstFighterBlocked = false;
let isSecondFighterBlocked = false;

let firstFighterRemainingHealth: number;
let secondFighterRemainingHealth: number;

let firstFighterIndicator: HTMLElement | null;
let secondFighterIndicator: HTMLElement | null;

let firstFighterOrigin: FighterDetail
let secondFighterOrigin: FighterDetail;
let resolveFn: (arg0?: FighterDetail) => void;
let comboKeys: {[key: string]: boolean} = {};
let comboTimeFirstFighter = 0;
let comboTimeSecondFighter = 0;

export async function fight(firstFighter: FighterDetail, secondFighter: FighterDetail): Promise<FighterDetail> {
  return new Promise((resolve: () => void) => {
    firstFighterOrigin = firstFighter;
    secondFighterOrigin = secondFighter;

    isFirstFighterBlocked = false;
    isSecondFighterBlocked = false;

    firstFighterRemainingHealth = firstFighter.health;
    secondFighterRemainingHealth = secondFighter.health;

    firstFighterIndicator = document.getElementById('left-fighter-indicator');
    secondFighterIndicator = document.getElementById('right-fighter-indicator');

    resolveFn = resolve;

    document.addEventListener('keydown', keydownHandler);

    document.addEventListener('keyup', keyupHandler);

    document.addEventListener('keypress', criticalKeydownHandler);
    document.addEventListener('keydown', criticalKeyupHandler);
  });
}

export function getDamage(attacker: FighterDetail, defender: FighterDetail): number {
  let damage = getHitPower(attacker) - getBlockPower(defender);

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: FighterDetail) {
  let criticalHitChance = Math.random() + 1;

  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: FighterDetail) {
  let dodgeChance = Math.random() + 1;

  return fighter.defense * dodgeChance;
}

function keydownHandler(event: KeyboardEvent): void {
  if (event.code === controls.PlayerOneBlock) {
    isFirstFighterBlocked = true;
  }

  if (event.code === controls.PlayerTwoBlock) {
    isSecondFighterBlocked = true;
  }

  if (event.code === controls.PlayerOneAttack && !isFirstFighterBlocked && !isSecondFighterBlocked) {
    secondFighterRemainingHealth -= getDamage(firstFighterOrigin, secondFighterOrigin);

    if (secondFighterIndicator === null) {
      throw new Error('Second fighter indicator does not exist');
    }

    secondFighterIndicator.style.width = secondFighterRemainingHealth * 100 / secondFighterOrigin.health + '%';

    if (secondFighterRemainingHealth <= 0) {
      secondFighterIndicator.style.width = '0';
      document.removeEventListener('keydown', keydownHandler);
      document.removeEventListener('keyup', keyupHandler);
      document.removeEventListener('keydown', criticalKeydownHandler);
      document.removeEventListener('keyup', criticalKeyupHandler);
      return resolveFn(firstFighterOrigin);
    }
  }

  if (event.code === controls.PlayerTwoAttack && !isSecondFighterBlocked && !isFirstFighterBlocked) {
    firstFighterRemainingHealth -= getDamage(secondFighterOrigin, firstFighterOrigin);

    if (firstFighterIndicator === null) {
      throw new Error('First fighter indicator does not exist');
    }

    firstFighterIndicator.style.width = firstFighterRemainingHealth * 100 / firstFighterOrigin.health + '%';

    if (firstFighterRemainingHealth <= 0) {
      firstFighterIndicator.style.width = '0';
      document.removeEventListener('keydown', keydownHandler);
      document.removeEventListener('keyup', keyupHandler);
      document.removeEventListener('keydown', criticalKeydownHandler);
      document.removeEventListener('keyup', criticalKeyupHandler);
      return resolveFn(secondFighterOrigin);
    }
  }
}

function keyupHandler(event: KeyboardEvent): void {
  if (event.code === controls.PlayerOneBlock) {
    isFirstFighterBlocked = false;
  }

  if (event.code === controls.PlayerTwoBlock) {
    isSecondFighterBlocked = false;
  }
}

function criticalKeydownHandler(event: KeyboardEvent) {
  if (controls.PlayerOneCriticalHitCombination.includes(event.code) || controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
    comboKeys[event.code] = true;
  }

  if (controls.PlayerOneCriticalHitCombination.every((key) => comboKeys[key]) && Date.now() - comboTimeFirstFighter > 10000) {
    secondFighterRemainingHealth -= 2 * firstFighterOrigin.attack;

    if (secondFighterIndicator === null) {
      throw new Error('First fighter indicator does not exist');
    }

    secondFighterIndicator.style.width = secondFighterRemainingHealth * 100 / secondFighterOrigin.health + '%';
    comboTimeFirstFighter = Date.now();
  }

  if (controls.PlayerTwoCriticalHitCombination.every((key) => comboKeys[key]) && Date.now() - comboTimeSecondFighter > 10000) {
    firstFighterRemainingHealth -= 2 * secondFighterOrigin.attack;

    if (firstFighterIndicator === null) {
      throw new Error('First fighter indicator does not exist');
    }

    firstFighterIndicator.style.width = firstFighterRemainingHealth * 100 / firstFighterOrigin.health + '%';
    comboTimeSecondFighter = Date.now();
  }
}

function criticalKeyupHandler(event: KeyboardEvent) {
  if (controls.PlayerOneCriticalHitCombination.includes(event.code) || controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
    comboKeys[event.code] = false;
  }

}
