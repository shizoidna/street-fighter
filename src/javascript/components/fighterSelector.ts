import { createElement } from '../helpers/domHelper';
import { Fighter, FighterDetail } from '../helpers/mockData';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from "../services/fightersService";
import versusImg from '../../../resources/versus.png';

export function createFightersSelector(): (event: Event, fighterId: string) => Promise<void> {
  let selectedFighters: FighterDetail[] = [];

  return async (event: Event, fighterId: string) => {
    const fighter: Fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo]: FighterDetail[] = selectedFighters;
    const firstFighter: FighterDetail = playerOne ?? fighter;
    const secondFighter: FighterDetail = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string) {
  if(!fighterDetailsMap.has(fighterId)) {
    const fighterInfo = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterInfo);
  }

  return fighterDetailsMap.get(fighterId);
}

function renderSelectedFighters(selectedFighters: FighterDetail[]): void {
  const fightersPreview: HTMLElement | null = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
  const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
  const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

  if(fightersPreview === null) {
    throw new Error('Fighters preview does not exist.')
  }

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: FighterDetail[]): HTMLElement {
  const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
  const onClick: () => void = () => startFight(selectedFighters);
  const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image: HTMLElement = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn: string = canStartFight ? '' : 'disabled';
  const fightBtn: HTMLElement = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: FighterDetail[]): void {
  renderArena(selectedFighters);
}
