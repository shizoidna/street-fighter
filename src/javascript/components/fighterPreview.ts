import { DOMElementArgumentAttributes } from '../entities/dom-element-argument-attributes';
import { createElement } from '../helpers/domHelper';
import { FighterDetail } from '../helpers/mockData';

export function createFighterPreview(fighter: FighterDetail, position: string) {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter === undefined) {
    return fighterElement;
  }

  const visibleProps: string[] = ['name', 'health', 'attack', 'defense', 'source'];

  let result = visibleProps.reduce((acc, key) => {

    const element: HTMLElement = createElement({
      tagName: 'div',
      className: 'fighter-preview-info'
    });

    let content;

    if (key === 'source') {
      content = createElement({
        tagName: 'img',
        className: 'fighter-preview-image',
        attributes: {
          src: fighter.source,
          alt: 'fighter-preview'
        }
      });
    }
    else {
      content = `${key}: ${fighter[key as keyof FighterDetail]}`;
    }

    element.append(content);
    acc.append(element);
    return acc;
  }, fighterElement);

  return fighterElement;
}

export function createFighterImage(fighter: FighterDetail) {
  const {source, name} = fighter;
  const attributes: DOMElementArgumentAttributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement: HTMLElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
