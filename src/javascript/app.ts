import { createFighters } from './components/fightersView';
import { Fighter } from './helpers/mockData';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement: HTMLElement | null = document.getElementById('root');
  static loadingElement: HTMLElement | null = document.getElementById('loading-overlay');

  async startApp() {
    if(App.rootElement === null || App.loadingElement === null) {
      throw new Error(`Failed to load data. Loading element doesn't exist.`)
    }

    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters: Fighter[] = await fighterService.getFighters();
      const fightersElement: HTMLElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
