import { DOMElementArgumentAttributes } from './dom-element-argument-attributes';

export interface DOMElementArgument {
    tagName: string;
    className?: string,
    attributes?: DOMElementArgumentAttributes
}
