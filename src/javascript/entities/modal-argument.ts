export interface modalArgument {
    title: string;
    bodyElement: string;
    onClose?: () => void;
}
