export interface SelectFighter {
    (event: Event, fighterId: string):  Promise<void>;
}
