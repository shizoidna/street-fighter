export interface ImageAttributes {
    src: string;
    title: string;
    alt: string;
}
