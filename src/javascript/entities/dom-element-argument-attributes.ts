export interface DOMElementArgumentAttributes {
    id?: string;
    src?: string;
    title?: string;
    alt?: string;
}
