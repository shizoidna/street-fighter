import { callApi } from '../helpers/apiHelper';
import { Fighter, FighterDetail } from '../helpers/mockData';

class FighterService {
  async getFighters(): Promise<Fighter[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: Fighter[] = await callApi(endpoint, 'GET') as Fighter[];

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<FighterDetail> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult: FighterDetail = await callApi(endpoint, 'GET') as FighterDetail;

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService: FighterService = new FighterService();
